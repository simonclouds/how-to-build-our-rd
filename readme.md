HOW TO BUILD OUR R&D
===
[toc]

研发过程中内部的协作流程、规范，与其他部门协作的流程说明。

## 1. worktile 使用
### 1.1 planning 项目
- [ ] [使用 worktile 管理冲刺周期需求](feature-from-worktile-to-gitlab.md)

## 2. gitlab 使用
### 2.1 管理出代码外的资源
使用 gitlab 管理研发资源的变更。
- [x] [管理原型导出 html 文件](how-to-manage-change-of-prototype.md)
- [x] [管理设计导出图片文件](how-to-manage-change-of-design.md)
- [x] [管理接口设计文档](how-to-manage-change-of-api-document.md)
- [x] [管理数据库设计文档](how-to-manage-change-of-db-design.md)
- [ ] 管理测试用例

### 2.2 管理开发项目 branch
统一的分支管理流程，方便管理和协作。

- [x] [如何使用 gitlab flow](how-to-use-gitlab-flow.md)

### 2.3 管理开发项目 commit
使用 git 进行团队协作，团队内遵循相同的 git 使用流程，避免杂乱无章的 commit，让团队成员可以顺畅的给任何项目做贡献。

- [x] [gitlab 中如何管理 commit](how-to-manage-commits.md)

### 2.4 推荐 comment 格式规范
comment 格式自由，推荐使用最广泛的规范，并可使用工具在此基础上自动生成 changelog。
- [x] [推荐的 comment 格式](comment-spec.md)

### 2.5 代码审查
强制增加环节：代码审查。提升代码质量。

- [x] [如何对项目进行代码审查](how-to-do-code-review.md)

### 2.6 milestone
归集功能开发和缺陷修复，与worktile 冲刺周期保持一致。

- [ ] [使用 milestone 管理冲刺周期](how-to-use-milestone.md)

### 2.7 tag 管理
让项目拥有规范的版本号命名规则。
- [x] [如何给项目打版本号](version-spec.md)

### 2.8 changelog
让每次程序版本的变更都有清晰明了的说明。
- [x] [如何自动生成格式优美的 changelog](changelog-spec.md)

### 2.9 使用 issue 
+ 使用 issue 串联起研发、测试、产品、设计、客服、技术支持等角色
+ 承担 bug 管理，工单管理，话题讨论，新功能开发等功能
+ 且让所有与 issue 相关的负责人得到实时的状态变更

- [ ] [如何使用好 issue](how-to-manage-issues.md)

### 2.10 工单管理
使用工单为客服、技术支持、内容运营等部分提供有问必答且高效的异步协作方式。
- [x] [如何使用工单进行研发对外的协作](how-tickets-system-work.md)

### 2.11 discuss
> 讨论比代码更重要，应该花更多的时间。

+ 开发前讨论需求的实现逻辑，新建 feature
+ 开发后提交代码讨论代码质量，code review

### 2.11 使用 wiki 管理公开文档

## 3. 如何使用钉钉
+ issue 变更
+ merge request 变更
+ commit

## 4. 工作习惯

## 待办事项
- [x] 1. gitlab 统一管理所有研发资源
- [ ] 2. gitlab 项目协作流程调整
- [ ] 3. 工单系统协作流程实施
- [ ] 4. merge request/code review 实施
- [ ] 5. 资源权限最大化
- [ ] 6. 自动化测试-pipeline
- [ ] 7. 团队内分享工具及环境搭建
- [ ] 8. 团队后端实践环境搭建
- [ ] 9. ...
