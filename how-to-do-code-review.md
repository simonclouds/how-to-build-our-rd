how to do code review
===

## what is code review
一种属于 CS 领域从业者特有的高级思维互动。

## why code reivew
+ 这是正常的不可或缺的开发环节，以前被主动选择忽视
+ 保证代码质量
+ 开发技能分享
+ 尽早让“bug 现行”，将 bug 杀死在摇篮
+ thought's hack & test & update

## how to do code review
### 现阶段(gitlab CE 版支持力度不够)
+ 将每个项目的 develop 变成保护分支
+ develop 分支的代码只能通过 merge request 进入，不能直接 commit 或 merge
    > 即在本地将新建分支 merge 到 develop，然后再 push 到主干的 develop 分支的方法将失效
+ 发起 merge request 者，邀请 reviewer ，2人+
+ merge request 的单位是一次分支合并，最小粒度是一个 bug 修复，或一个 feature 上线
+ 今日事今日毕。当天的 merge request ，当天做好 code review
+ 钉钉群中召集，然后 face2face
+ merge request 提出者输出为主

## 小原则（ from 豆瓣）
+ 尊重他人，就事论事，对事不对人，毕竟每个人都写过烂代码
+ PR 中的每一个 commit log 都应该可以和代码对应，方便 review
+ 尽量不要发太大的 PR，以免引起 reviewer 的恐慌
+ 建议保证一个 PR 的粒度和专注，最好不要出现一个 PR 里又有重构又加新 feature 的情况，同样容易引起 reviewer 的恐慌
+ 提 PR 之前请确保在本地或测试环境一切正常
+ reviewee 如果接受 reviewer 提出的修改意见，需要在修改提交以后知晓 reviewer，常见的做法可以是在 review comment 处回复（并带 commit 链接）
+ 评论中至少出现一个 lgtm 且保证 ci 通过之后 PR 才可以被合并；（注：lgtm 即 looks good to me 的缩写）
+ ~~PR 合并者与提交者不能是同一个人~~
+ PR 需从一个bug/feature分支（分支的名字尽量能表达代码的功能）发往上游的 develop 分支
+ ~~Model 的部分，如不紧急需要unittest~~
+ ~~Web 的部分，如不紧急需要webtest~~
+ PR 合并后如引起 bug 或功能异常，并经查确为此 PR 引起，提交者需请全组攻城湿喝饮料或吃冰棍（由被请者决定）
+ ~~将 fork 的仓库与上游同步时，应使用 git fetch upstream && git rebase upstream/master （或 code sync -r ），而不是 git merge 或 code sync （这里code是指面向 CODE 系统的一个命令行工具），以保持清晰的提交历史，并防止覆盖他人的修改~~
+ ~~注意安全问题，对于 SQL 拼字符串，模版中有 |n 的，以及处理用户输入等地方都需要仔细review，更多请参考 Web 安全开发指南~~


### 后阶段方向
+ 异步●并行●自动化
+ merge approval
+ lgtm
+ squash commits when merge
+ reviewer 输出为主

---
1. [豆瓣 CODE 两年历程回顾：git 不是万能的，没有 review 是万万不能的](http://www.infoq.com/cn/articles/douban-code-2years)