comment spec
===
编写逻辑清晰，格式规范的 comment 以方便：
+ 代码审查
+ 自动生成 ChangeLog
+ 快速展示开发者的思维逻辑(comment 内容主要说明为什么要做这次提交)
+ 是高效讨论的基础之一
    + 让自己的代码更容易理解
    + 让其他人能快速“入戏”

## 推荐规范
Commit message 都包括三个部分：Header，Body 和 Footer。
```
<type>[<scope>]: <subject>
// 空一行
[<body>]
// 空一行
[<footer>]
```

+ Header 必须，Body 和 Footer 可以省略
+ 每行字符数 <= 72
+ type
    + feat: feature
    + fix: bug
    + docs: documentation
    + style: 代码格式(不影响代码运行的变动)
    + refactor: 重构(不是feature和bug 的代码变动)
    + test: 增加测试
    + chore: 构建过程或辅助工具的变动
+ scope
    > scope用于说明 commit 影响的范围，比如数据层、控制层、视图层等等，视项目不同而不同。
+ subject
    > commit 目的的简短描述。
+ body
    > commit 详细描述
    + 说明代码变化的原因，以及与以前行为的对比
+ footer
    + 不兼容变动
        > 当前代码与上一个版本不兼容，以 `BREAKING CHANGE` 开头
    + 关闭 issue
        > 当前 commit 针对某个 issue ,则可以关闭。


## 在 comment 中操作 issue

在 commit 或 merge 时，comment 内容中匹配有匹配如下 
```
((?:[Cc]los(?:e[sd]?|ing)|[Ff]ix(?:e[sd]|ing)?|[Rr]esolv(?:e[sd]?|ing)|[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?%{issue_ref}(?:(?:, *| +and +)?)|([A-Z][A-Z0-9_]+-\d+))+)
``` 
正则的时候，会关闭该指定的 issue。

正则表达式的匹配单词如下：
+ Close, Closes, Closed, Closing, close, closes, closed, closing
+ Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing
+ Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved, resolving
+ Implement, Implements, Implemented, Implementing, implement, implements, implemented, implementing

注意，%{issue_ref} 是一个 gitlab 中复杂的匹配表达式，可匹配如下三种情况： 
1. 当前项目的issue (#123)
2. 跨项目的issue (group/project#123)
3. issue 的链接 (https://gitlab.example.com/group/project/issues/123).

举例：

```
fix(home): 修复下一步 button 跳转失败的问题

改变页面跳转的路由方式，从 **** --> ****。

close # 2
```

会关闭 #2 issue。


强烈推荐：

**如果本次的 commit 是针对某个 issue 的，并且完成了相应功能，就应该在其 comment 中关闭该 issue。** 
这样保证了每次 commit 的代码清晰明了，方便 code review 和代码分析。

注意，**只有当该 commit 最终合并到 gitlab 设置的默认分支上时，才会生效**。

---
参考文档：
1. [Commit message 和 Change log 编写指南](http://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)
2. [google angular commit](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#heading=h.greljkmo14y0)
