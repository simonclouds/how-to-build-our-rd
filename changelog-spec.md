changelog spec
===

changlog 用于版本发布时，公布软件较之于上一个版本，用户会感受到的变更；同时也是软件研发过程中的里程碑式的说明文档。

## 1. 规范
comment 内容按照之前的格式定义完成后，直接使用工具 `conventional-changelog` 生成 changelog 的内容。在工程中 CHANGELOG.md 保存内容。

### 1.1 安装 conventional-changelog
```
$ npm install -g conventional-changelog
$ cd my-project
```
### 1.2 生成 changelog
```
$ conventional-changelog -p angular -i CHANGELOG.md -s
```
---
1. [Commit message 和 Change log 编写指南](http://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)