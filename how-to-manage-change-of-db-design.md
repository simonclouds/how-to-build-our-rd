how to manage change of db design
===

## 如何对数据库设计变更进行管理
+ 数据库设计 power designer 的设计文件保存 *.pdm
+ 将数据库的结构导出到一个单一的 *.sql 文件
+ 对该 *.pdm 和 *.sql 文件放在 git 库中进行版本管理
+ 若要对数据库设计进行变更，必须先对该 *.pdm 文件进行变更，导出 *.sql 让相关人员知晓，不接受相关人员不知晓的情况下在线上生产环境直接对表结构进行修改
+ 数据库设计变更流程
    1. 从 git 主干库中拉 pdm 和 sql 文件到本地
    2. 新建分支并修改 pdm 文件中指定的表结构，导出相应的 sql 文件，并在 commit 时记录为什么要做本次变更
    3. 从主干中同步一次，并解决冲突
    4. 将新分支 push 到主干库
    5. 从 gitlab 界面中提起 merge request，并邀请相关人员参与 code review
    6. face2face 进行 code review
    7. merge 到 master
    8. 修改正式环境中的表结构