how to use git spec
===

[toc]

## 什么是 git 使用规范流程
研发团队多个成员，如何对多个项目多做贡献，共同提升，且保证代码质量，需要有一个 git 的使用规范，大家约定好，共同遵守 —— commit 管理。

![commit-manage]()

## 如何使用
### 1. fork
从 gitlab 群组中的项目 repository fork 到 gitlab 个人 remote repository 中。
![gitlib-fork-from-remote-repository](images/gitlib-fork-from-remote-repository.gif)

### 2. clone
将代码从个人的 remote repository 中 clone 到开发环境的 local repository。

在开发环境的主机上执行命令：
```
git clone http://git.newhigh.net/simon/ipynb.git
Cloning into 'ipynb'...
remote: Counting objects: 139, done.
remote: Compressing objects: 100% (67/67), done.
remote: Total 139 (delta 65), reused 139 (delta 65)
Receiving objects: 100% (139/139), 21.76 KiB | 1.21 MiB/s, done.
Resolving deltas: 100% (65/65), done.
```

### 3. checkout -b <new-branch-name>
+ 每次开发新的功能，都应该新建一个单独的分支。develop 为开发分支，所以应该从本地 develop 分支分出新的feature 分支进行开发。
+ 先更新本地 develop 分支
+ 分支命名以 issue 号开头，- 分割单词，bug/feature，再跟几个词语简单描述功能
```
$ cd ipynb
$ git branch
  develop
* master

$ git checkout develop
Switched to branch 'develop'
Your branch is up-to-date with 'origin/develop'.

$ git pull
Already up-to-date.

$ git checkout -b 2-feature-add-access-button
Switched to a new branch '2-feature-add-access-button'

```
### 4. commit
+ 在分支 `2-feature-add-access-button`上进行开发可多次 commit
+ 每次 commit 后，代码都必须保证库是一个完整可运行状态(不能 commit 不可用的代码)
+ 每次 commit 应该尽可能是完成一件特定的事：重构了功能代码/修改了文档/修复了指定bug/修改了接口 等
+ comment 遵循规范
```
git add .
git status
git commit --verbose
```
使用 `git commit --verbose` 进入详细的 comment 编辑状态，按照 comment 规范填写。

```
feat: add access button

changes:
增加新功能，访问按钮，方便用户一键获得资料。

close #2

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch 2-feature-add-access-button
# Changes to be committed:
#	modified:   readme.md
#
# ------------------------ >8 ------------------------
# Do not touch the line above.
# Everything below will be removed.
diff --git a/readme.md b/readme.md
index bc9c638..7c9f4c2 100644
--- a/readme.md
+++ b/readme.md
@@ -31,3 +31,5 @@ add test text
 
 ## from my own repo
 test
+
+## new line for test
```
在首行内按 comment 格式填写内容。`--verbose` 参数在上面的窗口中展示出了本次修改的 diff，在 comment 的详细描述部分解释为什么要做本次修改。

### 5. fetch
在开发过程中应该与常与公司项目主干的 remote repository 保持同步，尽可能让冲突尽早发生和解决。
```
$ git remote add upstream http://git.newhigh.net/open-docs/ipynb.git

$ git remote -v
origin  http://git.newhigh.net/simon/ipynb.git (fetch)
origin  http://git.newhigh.net/simon/ipynb.git (push)
upstream        http://git.newhigh.net/open-docs/ipynb.git (fetch)
upstream        http://git.newhigh.net/open-docs/ipynb.git (push)

```
切换回 develop 分支，同步项目主干远程库：
```
$ git checkout develop
Switched to branch 'develop'
Your branch is up-to-date with 'origin/develop'.
```
如果项目主干远程库中的 develop 分支和个人本地库中 develop 没有差异：
```
$ git pull upstream develop:develop
Already up-to-date.
```
如果存在差异：
```
$ git pull upstream develop:develop
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 2), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From http://git.newhigh.net/open-docs/ipynb
   4f14855..1420d4b  develop    -> develop
   4f14855..1420d4b  develop    -> upstream/develop
warning: fetch updated the current branch head.
fast-forwarding your working tree from
commit 4f1485587b380102f0a5eca3fc7f7354097e7e7a.
Already up-to-date.
```
将其更新到 feature 分支，并解决冲突：
```
$ git checkout 2-feature-add-access-button
Switched to branch '2-feature-add-access-button'

$ git merge develop
Auto-merging readme.md
CONFLICT (content): Merge conflict in readme.md
Automatic merge failed; fix conflicts and then commit the result.
$ vim readme.md
```
冲突片段如下，冲突如下：
```
## from my own repo
test
<<<<<<< HEAD

## new line for test
=======
## test line for conflict
>>>>>>> develop
```
解决冲突：
```

## from my own repo
test
## test line for conflict

```
保存后。
```
$ git add .
$ git commit
$ git status
On branch 2-feature-add-access-button
nothing to commit, working tree clean

$ git checkout develop
Switched to branch 'develop'
Your branch is ahead of 'origin/develop' by 1 commit.
  (use "git push" to publish your local commits)

$ git merge 2-feature-add-access-button
Updating 1420d4b..55a0b73
Fast-forward
 readme.md | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

```

### 6. rebase
### 7. push
```
$ git push origin develop:develop
Counting objects: 9, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (9/9), done.
Writing objects: 100% (9/9), 901 bytes | 901.00 KiB/s, done.
Total 9 (delta 6), reused 0 (delta 0)
To http://git.newhigh.net/simon/ipynb.git
   4f14855..55a0b73  develop -> develop
```

### 8. merge request
在个人远程库中，对公司项目主干远程库，提交 merge request.

![gitlib-submit-merge-request](images/gitlib-submit-merge-request.gif)