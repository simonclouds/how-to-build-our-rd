how to manage change of design
===

## 已实现部分
+ 使用 ps 进行设计
+ 从 ps 设计源文件导出图片文件
+ 将导出的图片文件使用 git 库进行版本管理
+ 可直接从 gitlab 平台的功能查看本次提交设计图上的变更
+ 对设计图有任何质疑，对该 git 库提出 issue ，并以此为讨论的基础
+ 提出 issue 并讨论，应该在每个功能的设计完成时
+ 单个 feature 为最小提交粒度
+ 研发所有相关成员均可提 issue