how to manage change of api document
===

## 已实现
+ 统一使用 postman 作为 api 文档编辑和呈现的工具
+ 对 postman 的导出 collection.json 文档进行版本管理
+ 后端每个单独运行的服务一个 collection 保存
+ 必须先有确定的达成一致的 api 设计文档，再进行开发
+ api 设计文档的每次变更必须经过 code review，达成一致
+ api 的设计文档只能通过 merge 进入主干的 master 分支
+ api 设计文档变更的流程
    1. 将 json 文件 clone 到本地
    2. 新建变更分支
    3. 将新分支的 json 文件导入 postman
    4. api 设计变更
    5. 从 remote 的 master 分支同步一次，解决冲突
    6. 将本地的变更分支 push 到 remote
    7. 发起 merge request，并邀请前后端相关成员参与 code review
    8. 面对面 code review 并 merge