how to use gitlab flow
===
[toc]

## 1. what is gitlab flow
一种 git **分支的管理方式**。

## 2. why gitlab flow
+ 统一分支管理方式，项目间协作更流畅
+ 有适应不同开发环境的弹性，又有单一主分支的简单和便利
+ Gitlab.com 推荐的做法
+ 恰好和我们研发的流程非常接近
+ 可以做到以功能为最小单位的代码撤销

## 3. how to use gitlab flow

![branch-manage](images/gitlab-branch-manage.png)

+ develop/release/master 是环境分支，且受保护，只能通过 merge request 的方式更新
+ feature/bug 分支是功能分支，不是保护分支，可直接 commit 更新

### 3.1 上游优先
Gitlab flow 的最大原则叫做"上游优先"（upsteam first），即只存在一个主分支 **develop**，它是所有其他分支的"上游"。只有上游分支采纳的代码变化，才能应用到其他分支。
推荐上游分支依次为:
1. develop 开发的主分支
2. release 预发布分支
3. master 生产环境分支

### 3.2 持续发布
如果生产环境出现了 bug，就新建 bug 分支，修复完成后，merge 到 develop 分支，测试没问题，再合并到 release 分支，类生产环境的 release 也没问题，才 合并到 master 分支，打tag。

**没有特殊情况，禁止直接跳过上游，给下游更新代码。**


## 4. 技巧
1. develop 分支都是保护分支，只能通过 merge request 将功能分支的代码更新到此分支（**Pull Request本质是一种对话机制，你可以在提交的时候，@相关人员或团队，引起他们的注意。**）
2. 只有项目管理员才有对保护分支管理的权利，即审批是否通过 merge request。
3. 功能分支都是为了解决一个或多个 issue，可以是 bug，也可以是 feature。命名方式：issue编号-issue类型-issue简称，如：`15-bug-login-button-jump-failed`
4. bug 功能分支在 merge 后，立即被删除
5. feature 功能分支在 merge 后，保留，直到该 feature 功能上线到生产环境后再删除
