<a name=""></a>
#  (2018-03-15)


### Features

* test changelog ([da81f6d](http://git.newhigh.net/open-docs/how-to-build-our-rd/commits/da81f6d))



<a name=""></a>
#  (2018-03-15)


### Bug Fixes

* test bug fix ([4ca912a](http://git.newhigh.net/open-docs/how-to-build-our-rd/commits/4ca912a))


### Features

* add how to manage branch and commit ([af437a9](http://git.newhigh.net/open-docs/how-to-build-our-rd/commits/af437a9))
* add readme.md ([e6fb32a](http://git.newhigh.net/open-docs/how-to-build-our-rd/commits/e6fb32a))
* add root.md for main structure of all documents ([e43d2c3](http://git.newhigh.net/open-docs/how-to-build-our-rd/commits/e43d2c3))
* add several *.md ([d5c5861](http://git.newhigh.net/open-docs/how-to-build-our-rd/commits/d5c5861))
* add some edraw files ([79f5c9b](http://git.newhigh.net/open-docs/how-to-build-our-rd/commits/79f5c9b))
