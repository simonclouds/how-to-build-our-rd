how to manage change of prototype
===

## 已实现部分
+ prototype 使用 axure 工具设计
+ axure 的原型源文件直接导出 html 格式
+ 将 html 格式文件使用 git 库保存
+ 同时为所有 html 搭建一个静态网站，并提供全网的加密访问
+ 使用 docker 部署该静态网站，每次推送 html 到 git 库后，自动部署
+ 对 prototype 如有质疑，对该 git 库提 issue，后续的讨论以该 issue 为基础
+ 提出 issue 并讨论，应该在每个功能的原型完成时
+ 单个 feature 为最小提交粒度
+ 研发所有相关成员均可提 issue

## 尚未实现部分
+ 每次更新 prototype 时，对 html 做改造，以满足可以可视化看出原型的变更
+ 保留多个历史版本以做比较