how to manange commits
===

[toc]

## commit 管理
研发团队多个成员，如何对多个项目多做贡献，共同提升，且保证代码质量，需要有一个 git 的使用规范，大家约定好，共同遵守 —— commit 管理。

## 如何使用

![commit-manage](images/gitlab-commit-manage.png)

### 1. clone
将代码从主干 remote repository 中 clone 到开发环境的 local repository。

在开发环境的主机上执行命令：
```
git clone http://git.newhigh.net/simon/ipynb.git
Cloning into 'ipynb'...
remote: Counting objects: 139, done.
remote: Compressing objects: 100% (67/67), done.
remote: Total 139 (delta 65), reused 139 (delta 65)
Receiving objects: 100% (139/139), 21.76 KiB | 1.21 MiB/s, done.
Resolving deltas: 100% (65/65), done.
```

### 2. checkout -b new-branch-name
+ 每次开发新的功能，都应该新建一个单独的分支。develop 为开发分支，所以应该从本地 develop 分支分出新的 feature 分支进行开发。
+ 先更新本地 develop 分支
+ 分支命名以 issue 号开头，- 分割单词，bug/feature，再跟几个词语简单描述功能

```
$ cd ipynb
$ git branch
  develop
* master

$ git checkout develop
Switched to branch 'develop'
Your branch is up-to-date with 'origin/develop'.

$ git pull
Already up-to-date.

$ git checkout -b 2-feature-add-access-button
Switched to a new branch '2-feature-add-access-button'

```
### 3. commit
+ 在分支 `2-feature-add-access-button`上进行开发可多次 commit
+ 每次 commit 后，代码都必须保证库是一个完整可运行状态(不能 commit 不可用的代码)
+ 每次 commit 应该尽可能是完成一件特定的事：重构了功能代码/修改了文档/修复了指定bug/修改了接口 等，查看 [comment-spec.md](comment-spec.md) 中 comment 的 type 字段
+ comment 遵循规范，查看 [comment-spec.md](comment-spec.md)

```
git add .
git status
git commit --verbose
```
使用 `git commit --verbose` 进入详细的 comment 编辑状态，按照 [comment 规范](comment-spec.md)填写。
`--verbose` 参数在上面的窗口中展示出了本次修改的 diff，在 comment 的详细描述部分解释为什么要做本次修改。

### 5. pull
做一次同步，如果主干 remote repository 的 develop 分支代码发生了变化，需要同步到 local repository 的 develop 分支。
```
$ git checkout develop
$ git pull origin develop:develop
```
### 6. merge
若 develop 有更新，则同步到本地新建分支 `2-feature-add-access-button` 上，并解决冲突
```
$ git checkout 2-feature-add-access-button
$ git merge develop
```
### 7. **push** *
将本地分支推送到主干 remote repository 新建分支 2-feature-add-access-button 。
```
git push --upstream origin 2-feature-add-access-button
```
### 8. **merge request** *
![提交merge request](images/gitlib-submit-merge-request.gif)