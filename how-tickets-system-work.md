how tickets system work
===

[toc]

## 1. what is tickets system
工单系统。

## 2. why tickets system
+ 记录所有能记录的问题和中间过程
+ 所有问题有问必答
+ 让问题数量收敛
+ 将研发与其他部分的工作解耦，异步进行

## 3. how it works
+ 项目 —— open-docs/wiki
+ 钉钉群 —— 【流海研发 gitlab 推送】
+ 参与者 —— 客服、技术支持、内容运营、产品、设计、研发、测试
+ 每次 issue 状态变更，会在【流海研发 gitlab 推送】中通知，并@相关参与者，提醒其关注

工单在不同参与者之间的流转示意图：

![how-tickets-system-work](images/tickets-system-working-with-rd.png)

1. 工单创建者接到问题反馈，在 gitlab 上创建 issue
2. gitlab 自动通知【流海研发 gitlab 推送】中工单负责人
3. 工单负责人第一时间对该 issue 做出判断，标记为已接单
4. 工单负责人判断是否需要额外的信息，在该 issue 下进行评论，并在【流海研发 gitlab 推送】@ 工单创建者
5. 工单创建者提供额外的信息
6. 工单负责人预估处理时间，进入 issue 处理阶段
7. 工单处理完成，关闭 issue，并在【流海研发 gitlab 推送】@工单创建者
8. 工单创建者回归测试
9. 回归测试通过，该 issue 则彻底关闭
10. 否则工单创建者重新打开该 issue ，在评论中说明理由，重复 3-9
